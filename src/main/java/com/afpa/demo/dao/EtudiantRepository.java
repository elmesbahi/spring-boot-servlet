package com.afpa.demo.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.demo.entity.EtudiantEntity;

@Repository
public interface EtudiantRepository extends CrudRepository<EtudiantEntity, Integer> {

}
