package com.afpa.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.demo.dao.EtudiantRepository;
import com.afpa.demo.dto.EtudiantDto;
import com.afpa.demo.entity.EtudiantEntity;

@Service
public class EtudiantService implements IEtudiantService {
	
	@Autowired
	private EtudiantRepository etudiantRepository;
	
	@Override
	public List<EtudiantDto> getEtudiantList(){
		List<EtudiantDto> res = new ArrayList<>();
		etudiantRepository.findAll().forEach(x->{
			res.add(EtudiantDto.builder().nom(x.getNom()).id(x.getId()).build());
		});
		return res;
	}

	public void create(EtudiantDto eDto) {
		this.etudiantRepository.save(EtudiantEntity.builder().nom(eDto.getNom()).build());
	}
}
