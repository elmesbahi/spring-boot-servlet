package com.afpa.demo.service;

import java.util.List;

import com.afpa.demo.dto.EtudiantDto;

public interface IEtudiantService {

	List<EtudiantDto> getEtudiantList();

}
