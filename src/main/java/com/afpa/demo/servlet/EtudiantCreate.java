package com.afpa.demo.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.demo.dto.EtudiantDto;
import com.afpa.demo.service.EtudiantService;

@WebServlet("/create")
public class EtudiantCreate extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private EtudiantService etudiantService;

	private ServletContext sc;

	@Override
	public void init(ServletConfig config) throws ServletException{
		sc = config.getServletContext();
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
		etudiantService = webApplicationContext .getBean(EtudiantService.class);
		System.out.println("\n-------\n"+etudiantService);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		System.out.println("MyServlet's doGet() method is invoked.");
		doAction(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		System.out.println("MyServlet's doPost() method is invoked.");
		doAction(req, resp);
	}

	private void doAction(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/plain");
		etudiantService.create(EtudiantDto.builder().nom("e"+System.currentTimeMillis()).build());
		resp.getWriter().write("creation ok ");
	}

}
