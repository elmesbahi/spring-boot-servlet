package com.afpa.demo.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EtudiantDto {
	private int id;
	private String nom;
}
