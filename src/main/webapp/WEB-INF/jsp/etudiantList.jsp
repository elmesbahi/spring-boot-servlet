<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Spring-boot demo</title>
</head>
<body class="ml-5 mt-5 mr-5">

	<table class="table table-stripe table-hover">
		<thead class="table-dark">
			<tr>
				<th>id</th>
				<th>nom</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${etudiantsList}" var="e" varStatus="status">
				<tr>
					<td><c:out value="${e.id}" /></td>
					<td><c:out value="${e.nom}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</body>
</html>